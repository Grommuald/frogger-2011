#include "frogger.h"

samochod::samochod(int start_x, int start_y, int start_predkosc, char start_kierunek, string kol)
{
 x = start_x;
 y = start_y;
 
 szerokosc = 64;
 wysokosc = 32;
 
 if(start_kierunek == 'p')
  velx = start_predkosc;
 else
  velx = -start_predkosc;
 
 predkosc = start_predkosc;
 kierunek = start_kierunek;
 kolor = kol;              
 
 if(kierunek == 'p' && kolor == "czerwony")
    tekstura = ladujTeksture("/res/czerwony_p.png");
 else if(kierunek == 'p' && kolor == "niebieski")
    tekstura = ladujTeksture("/res/niebieski_p.png");
 else if(kierunek == 'p' && kolor == "zielony")
    tekstura = ladujTeksture("/res/zielony_p.png");
 else if(kierunek == 'l' && kolor == "czerwony")
    tekstura = ladujTeksture("/res/czerwony_l.png");
 else if(kierunek == 'l' && kolor == "niebieski")
    tekstura = ladujTeksture("/res/niebieski_l.png");
 else if(kierunek == 'l' && kolor == "zielony")
    tekstura = ladujTeksture("/res/zielony_l.png");
 
 
               
}
samochod::~samochod()
{
 glDeleteTextures(1, &tekstura); 
}
void samochod::pokaz()
{
    glBindTexture(GL_TEXTURE_2D, tekstura);
    //glTranslatef(x, y, 0); 
    glBegin(GL_QUADS);
    
     glColor4f(1, 1, 1, 1);
     
     glTexCoord2d(0, 0); glVertex3f(x, y, 0);
     glTexCoord2d(1, 0); glVertex3f(x + szerokosc, y, 0);
     glTexCoord2d(1, 1); glVertex3f(x + szerokosc, y + wysokosc, 0);
     glTexCoord2d(0, 1); glVertex3f(x, y + wysokosc, 0);
    
    glEnd();
          
}

pas_drogi::pas_drogi(int s_y, int ekran_szer)
{
  y = s_y;

  //losujemy ilosc aut
  int ilosc_aut = (rand() % 7) + 1;
  //losujemy kierunek toru
  bool kierunek = rand() % 2;
  if(!kierunek)
     kierunek_toru = 'p';
  else if(kierunek)
     kierunek_toru = 'l';
  
  int predkosc_toru = (rand() % 6)+2; //2 - 6
  //ladujemy auta 
  for(int i = 0; i < ilosc_aut; ++i)
  {
    //ustawiamy ich kolor
    int kolor = rand() % 3;
    string kolor_tekst;
      
   if(kolor == 0) kolor_tekst = "zielony";
   else if(kolor == 1) kolor_tekst = "niebieski";
   else if(kolor == 2) kolor_tekst = "czerwony"; 
   
   int start_x = 0;
   if(kierunek_toru == 'l')
   {
     start_x = ekran_szer - 64;
     samochody.push_back(new samochod(start_x - 120*i, y, predkosc_toru, kierunek_toru, kolor_tekst));
   }
   if(kierunek_toru == 'p')
   {
     start_x = 0 ;
     samochody.push_back(new samochod(start_x + 120*i, y, predkosc_toru, kierunek_toru, kolor_tekst));
   }
  
  }
                 
}
pas_drogi::~pas_drogi()
{
 //usuwamy auta
 for(int i = 0; i < samochody.size(); ++i)
 {
  delete samochody[i];
 }                      
                          
}
void pas_drogi::pokazPas()
{
 for(int i = 0; i < samochody.size(); ++i)
 {
  samochody[i]->pokaz();
 }
}
void pas_drogi::sterujRuchem(int ekran_szer)
{
 
 for(int i = 0; i < samochody.size(); ++i)
 {

  samochody[i]->x += samochody[i]->velx;
  //sprawdzamy czy samochody nie wysz�y poza ekran
  if(kierunek_toru == 'p' && samochody[i]->x > ekran_szer)
     samochody[i]->x = 0 - samochody[i]->szerokosc;
  
  if(kierunek_toru == 'l' && samochody[i]->x + samochody[i]->szerokosc < 0)
     samochody[i]->x = ekran_szer;
 

 }
}
void pas_drogi::sprawdzKolizje(zaba *gracz)
{
 for(int i = 0; i < samochody.size(); ++i)
 {
   if(kolizja(samochody[i]->x, samochody[i]->y, samochody[i]->szerokosc, samochody[i]->wysokosc,
            gracz->x, gracz->y, gracz->szerokosc, gracz->wysokosc))
   {
     gracz->zycia -=1;
     gracz->x = gracz->start_x;
     gracz->y = gracz->start_y;       
   }
         
 } 
}
     
zaba::zaba(int s_x, int s_y, int s_zycia)
{
 x = start_x = s_x;
 y = start_y = s_y;
 zycia = s_zycia;
 
 zyje = true;
 szerokosc = 16;
 wysokosc = 16;
 
 punkty = 0;
 tekstura = ladujTeksture("/res/zaba.png");
           
}
zaba::~zaba()
{
 glDeleteTextures(1, &tekstura); 
}
void zaba::rusz(char kierunek, int ekran_szer, int ekran_wys)
{
 if(zyje)
 {
  if(kierunek == 'g' && y > 0)
     y -= 16;
  else if(kierunek == 'd' && y + wysokosc < ekran_wys)
     y += 16;
  else if(kierunek == 'p' && x + szerokosc < ekran_szer)
     x += 16;
  else if(kierunek == 'l' && x > 0)
     x -= 16;
  }
    
}
void zaba::rysuj()
{
    glBindTexture(GL_TEXTURE_2D, tekstura);
    glBegin(GL_QUADS);
    
     glColor4f(1, 1, 1, 1);
     
     glTexCoord2d(0, 0); glVertex3f(x, y, 0);
     glTexCoord2d(1, 0); glVertex3f(x + szerokosc, y, 0);
     glTexCoord2d(1, 1); glVertex3f(x + szerokosc, y + wysokosc, 0);
     glTexCoord2d(0, 1); glVertex3f(x, y + wysokosc, 0);
    
    glEnd();
}
punkty::punkty(int ekran_s, int ekran_w)
{
//nie losujemy zeby pokazalo sie na skrajnych punktach ekranu
 x = (rand() % ekran_s-32) + 32; 
 y = (rand() % ekran_w-32) + 32;
 
 szerokosc = 16;
 wysokosc = 16;
 
 tekstura = ladujTeksture("/res/punkt.png");
}
void punkty::losujPoz(int ekran_s, int ekran_w)
{
 x = (rand() % ekran_s-32) + 32; 
 y = (rand() % ekran_w-32) + 32;
      
}
void punkty::rysuj()
{
  glBindTexture(GL_TEXTURE_2D, tekstura);
    glBegin(GL_QUADS);
    
     glColor4f(1, 1, 1, 1);
     
     glTexCoord2d(0, 0); glVertex3f(x, y, 0);
     glTexCoord2d(1, 0); glVertex3f(x + szerokosc, y, 0);
     glTexCoord2d(1, 1); glVertex3f(x + szerokosc, y + wysokosc, 0);
     glTexCoord2d(0, 1); glVertex3f(x, y + wysokosc, 0);
    
    glEnd();    
     
}
frogger::frogger(int szerokosc_ekranu, int wysokosc_ekranu)
{                    
 error = false;
 tlo = 0;
 
 ekran_szer = szerokosc_ekranu;
 ekran_wys  = wysokosc_ekranu;
 
 if(inicjuj() == 1) error = true;
 if(inicjujGL() == 1) error = true;
 
 //inicjujemy ziarno losowosci
  srand(time(NULL));
 

 
  //tworzymy droge
 for(int i = 0; i < 8; ++i)
  droga[i] = new pas_drogi(32+(i*64), ekran_szer);
  
  //tworzymy zabe
  gracz = new zaba(ekran_szer/2, ekran_wys-16, 3); 
  //tworzymy monet�
  moneta = new punkty(ekran_szer, ekran_wys);
  
  tlo = ladujTeksture("/res/tlo.bmp");
  teksturaSerce = ladujTeksture("/res/serce.png");
  teksturaKoniec = ladujTeksture("/res/ekran_koncowy.png");
  czcionkaPunkty = TTF_OpenFont("/res/arial.ttf", 30);

}
frogger::~frogger()
{
 for(int i = 0; i < 8; ++i)
 delete droga[i];
 
 delete gracz;
 TTF_CloseFont(czcionkaPunkty);
 TTF_Quit();
 
 SDL_Quit();
                   
}
bool frogger::inicjuj()
{
 if(SDL_Init(SDL_INIT_EVERYTHING) == -1)return 1;
 if(SDL_SetVideoMode(ekran_szer, ekran_wys, 32, SDL_OPENGL) == NULL) return 1;
 if(TTF_Init() == -1)return 1;
 SDL_WM_SetCaption("Frogger", NULL);
  
 return 0;
}
bool frogger::inicjujGL()
{
   SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
   SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
   SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
   SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
   SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE, 32);
   SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);
   SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
 
   glClearColor(0, 0, 0, 1);
   
   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();
   glOrtho(0, ekran_szer, ekran_wys, 0, -1, 1);
   
   glMatrixMode(GL_MODELVIEW);   
   glLoadIdentity();
   
   
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    
    if(glGetError() != GL_NO_ERROR)
     return 1;
     
   return 0;
   
}
void frogger::logika()
{
  if(gracz->zycia <= 0)
   gracz->zyje = false;
  //wprawiamy auta w ruch
 if(gracz->zyje == true)
 {
  for(int i = 0; i < 8; ++i)
     droga[i]->sterujRuchem(ekran_szer);
  //sprawdamy kolizje zaby z autami
  for(int i = 0; i < 8; ++i)
     droga[i]->sprawdzKolizje(gracz);
  //jezeli kolidujemy sie z monet�
  if(kolizja(moneta->x, moneta->y, moneta->szerokosc, moneta->wysokosc,
             gracz->x, gracz->y, gracz->szerokosc, gracz->wysokosc))
  {
   gracz->punkty += 1;
   moneta->losujPoz(ekran_szer, ekran_wys);
   
  }
 }
     
}
void frogger::renderuj()
{
  glClear(GL_COLOR_BUFFER_BIT); 
   glLoadIdentity();
   glPushMatrix();
   glEnable(GL_TEXTURE_2D);
   
   //t�o 
     glBindTexture(GL_TEXTURE_2D, tlo);
     glBegin(GL_QUADS);
    
     glColor4f(1, 1, 1, 1);
     
     glTexCoord2d(0, 0); glVertex3f(0,0, 0);
     glTexCoord2d(1, 0); glVertex3f(ekran_szer, 0, 0);
     glTexCoord2d(1, 1); glVertex3f(ekran_szer, ekran_wys, 0);
     glTexCoord2d(0, 1); glVertex3f(0, ekran_wys, 0);
    
    glEnd();
   
   //gracz
   if(gracz->zyje)
   {
    gracz->rysuj();
    // i moneta
    moneta->rysuj();
   }
   
 
   //samochody
   for(int i = 0; i < 8; ++i)
     droga[i]->pokazPas();
   //serduszka
   glBindTexture(GL_TEXTURE_2D, teksturaSerce);
   glBegin(GL_QUADS);
   //glColor3f(0, 0, 0);
   
   for(int i = 0; i < gracz->zycia; ++i)
   {
     glTexCoord2d(0, 0); glVertex3f(i*32,0, 0);
     glTexCoord2d(1, 0); glVertex3f(i*32 + 32, 0, 0);
     glTexCoord2d(1, 1); glVertex3f(i*32 + 32, 32, 0);
     glTexCoord2d(0, 1); glVertex3f(i*32, 32, 0);     
   }
   glEnd();

  
   pokazNapis("Punkty:", gracz->punkty, ekran_szer - 150, 0);
   //je�eli dead to pokazujemy ekran porazki 
   if(!gracz->zyje)
   {
    int x_kon= (ekran_szer/2)-200;
    int y_kon= (ekran_wys/2)-150;
    
    glBindTexture(GL_TEXTURE_2D, teksturaKoniec);
    glBegin(GL_QUADS);
     glTexCoord2d(0, 0); glVertex3f(x_kon, y_kon, 0);
     glTexCoord2d(1, 0); glVertex3f(x_kon + 400, y_kon, 0);
     glTexCoord2d(1, 1); glVertex3f(x_kon + 400, y_kon + 300, 0);
     glTexCoord2d(0, 1); glVertex3f(x_kon, y_kon + 300, 0);  
    glEnd();
   }
   glDisable(GL_TEXTURE_2D);  
   
   glPopMatrix();
   glLoadIdentity();
    
    SDL_GL_SwapBuffers();
}
int frogger::start()
{
 if(error)return 1;
 
 bool czyDziala = true;
 while(czyDziala)
 {
  przechwycZdarzenie(czyDziala);
  
  logika();
  renderuj();
  
  ustawFPS(30);
                 
 }
 
 return 0;   
}
void frogger::restart()
{
 gracz->zyje = true;
 gracz->zycia = 3;
 gracz->punkty = 0;
 moneta->losujPoz(ekran_szer, ekran_wys);
     
}
void frogger::przechwycZdarzenie(bool &steruj)
{
 SDL_Event zdarzenie;
 while(SDL_PollEvent(&zdarzenie))
 {
  if(zdarzenie.type == SDL_QUIT)
   steruj = false;
  if(zdarzenie.type == SDL_KEYDOWN)
  {
   switch(zdarzenie.key.keysym.sym)
   {
      case SDLK_ESCAPE:
           steruj = false;
           break;
      case SDLK_UP:
        gracz->rusz('g', ekran_szer, ekran_wys);
           break;
      case SDLK_DOWN:
        gracz->rusz('d', ekran_szer, ekran_wys);   
           break;
      case SDLK_RIGHT:
        gracz->rusz('p', ekran_szer, ekran_wys);   
           break;
      case SDLK_LEFT:
        gracz->rusz('l', ekran_szer, ekran_wys);   
           break;
      case SDLK_t:
        if(!gracz->zyje)
          restart();
         break;
      case SDLK_n:
        if(!gracz->zyje)
          steruj = false;              
         break;
                                       
                                   
   }
  }
 }
}
     
void frogger::ustawFPS(int fps)
{
  static int nastepny = 0;
  int aktualny;
  
  aktualny = SDL_GetTicks();
  if(aktualny < nastepny)
     SDL_Delay(nastepny - aktualny);
  nastepny = SDL_GetTicks() + (1000/fps);
     
}

void frogger::pokazNapis(string tekst, int nr, int x, int y)
{
 SDL_Surface *napis= NULL;
 SDL_Color kolor = {255, 255, 255};
 stringstream tmp;
 tmp << tekst << nr;
 
 napis = TTF_RenderText_Blended(czcionkaPunkty, tmp.str().c_str(), kolor);
 unsigned tekstura = 0;
 
 glGenTextures(1, &tekstura);
 glBindTexture(GL_TEXTURE_2D, tekstura);
 
 glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
 glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
 
 glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, napis->w, napis->h, 0, GL_BGRA, GL_UNSIGNED_BYTE, napis->pixels);

 glBegin(GL_QUADS);
  glTexCoord2d(0, 0); glVertex3d(x, y, 0);
  glTexCoord2d(1, 0); glVertex3d(x+napis->w, y, 0);
  glTexCoord2d(1, 1); glVertex3d(x+napis->w, y+napis->h, 0);
  glTexCoord2d(0, 1); glVertex3d(x, y+napis->h, 0);
 glEnd();

 SDL_FreeSurface(napis);
     
}
GLuint ladujTeksture(const string &nazwa)
{
    GLuint texture;
    GLenum texture_format;
    GLint  nOfColors;
    SDL_Surface *src = IMG_Load(nazwa.c_str());
    
    
// Check that the image's width is a power of 2
if ( (src->w & (src->w - 1)) != 0 ) {
printf("warning: image's width is not a power of 2\n");
}

// Also check if the height is a power of 2
if ( (src->h & (src->h - 1)) != 0 ) {
printf("warning: image's height is not a power of 2\n");
}
        // get the number of channels in the SDL surface
        nOfColors = src->format->BytesPerPixel;
        if (nOfColors == 4)     // contains an alpha channel
        {
                if (src->format->Rmask == 0x000000ff)
                {
                     texture_format = GL_RGBA;
                }
                else
                {
                     texture_format = GL_BGRA_EXT;
                }
                printf("32bit\n");
        } else if (nOfColors == 3)     // no alpha channel
        {
                if (src->format->Rmask == 0x000000ff)
                        texture_format = GL_RGB;
                else
                        texture_format = GL_BGR_EXT;
                                printf("24bit\n");
        } else {
                printf("warning: the image is not truecolor..  this will probably break\n");
                // this error should not go unhandled
        }

// Have OpenGL generate a texture object handle for us
glGenTextures( 1, &texture );

// Bind the texture object
glBindTexture( GL_TEXTURE_2D, texture );

// Set the texture's stretching properties
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );

// Edit the texture object's image data using the information SDL_Surface gives us
glTexImage2D( GL_TEXTURE_2D, 0, nOfColors, src->w, src->h, 0,
                      texture_format, GL_UNSIGNED_BYTE, src->pixels );

 //   SDL_FreeSurface(src);
    return texture;
}
bool kolizja(int ax, int ay, int aw, int ah,
               int bx, int by, int bw, int bh)
{
   if(ay + ah < by)return false;
   else if(ay > by + bh)return false;
   else if(ax + aw < bx)return false;
   else if(ax > bx + bw)return false;
   
   return true;               
               
               
}
