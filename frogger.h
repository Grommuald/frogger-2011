#include <SDL/SDL.h>
#include <SDL/SDL_opengl.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>
#include <string>
#include <iostream>
#include <cstdlib>
#include <vector>
#include <time.h>
#include <sstream>
using namespace std;

GLuint ladujTeksture(const std::string &nazwa_pliku);
bool kolizja(int ax, int ay, int aw, int ah,
               int bx, int by, int bw, int bh);
class zaba
{
 private:
  unsigned int tekstura;
  
 public:
 int x, y;
 int szerokosc, wysokosc;
 int start_x;
 int start_y;
 
 int punkty;
 bool zyje;
 int zycia;
 
 zaba(int s_x, int s_y, int s_zycia);
 ~zaba();
 void rusz(char kierunek, int ekran_szer, int ekran_wys);
 void rysuj();
      
};



struct samochod
{
  int x, y;
  int velx;
  
  int predkosc;
  
  int szerokosc, wysokosc;
  unsigned int tekstura;
  
  string kolor;
  char kierunek;
  
  samochod(int start_x, int start_y, 
           int start_predkosc, char start_kierunek, string kol);
  ~samochod();
  void pokaz(); 
};

class pas_drogi
{
 vector<samochod*> samochody;
 
 char kierunek_toru;
 int x, y;

 public:
       
  pas_drogi(int s_y, int ekran_szer);
 ~pas_drogi();
  
  
  void pokazPas();
  void sterujRuchem(int ekran_szer);
  void sprawdzKolizje(zaba *);
  
            
      
};
struct punkty
{ 
 unsigned int tekstura;
 
 int x, y;
 int szerokosc, wysokosc;
 
 punkty(int ekran_s, int ekran_w); //w konstruktorze losujemy punkty
 
 void rysuj();
 void losujPoz(int ekran_s, int ekran_w);
       
};
class frogger
{
 int ekran_szer;
 int ekran_wys;
 
 bool error;
 
 bool inicjuj();
 bool inicjujGL();
 
 zaba *gracz;
 pas_drogi *droga[8];
 punkty *moneta;
 
 TTF_Font *czcionkaPunkty;
 
 unsigned int tlo;
 unsigned int teksturaSerce;
 unsigned int teksturaKoniec;
 unsigned int teksturaNapis;

 public:
   frogger(int szerokosc_ekranu, int wysokosc_ekranu);
  ~frogger();

  int start();
  void przechwycZdarzenie(bool &);
  void restart();
  
  void pokazNapis(string tekst, int nr, int x, int y);
  
  void renderuj();
  void logika();
  void ustawFPS(int fps);
             
};
